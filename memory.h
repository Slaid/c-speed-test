#include <tlhelp32.h>

char* m_pName = "eurotrucks2.exe";
int m_pID = 0;  

class memtest
{
    
public:
       
       
       HANDLE m_hProc;
       
       void getPID() {
        HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        PROCESSENTRY32 pInfo;
        pInfo.dwSize = sizeof(PROCESSENTRY32);
 
        if (Process32First(snapshot, &pInfo))
        {
                while (Process32Next(snapshot, &pInfo))
                {
                        if (_stricmp(m_pName, pInfo.szExeFile) == 0)
                        {
                                m_pID = pInfo.th32ProcessID;
                                CloseHandle(snapshot);
                                return;
                        }
                }
        }
        CloseHandle(snapshot);
        m_pID = 0;
      }
      
      void Open(DWORD accessRights = PROCESS_VM_READ)
      {       
        accessRights = PROCESS_ALL_ACCESS;
        getPID();
        m_hProc = OpenProcess(accessRights, false, m_pID);
      }
      
      byte*  m_buffer;
      size_t m_bufferSize;
      void Read(DWORD from, size_t size)
      {
            m_buffer = new byte[size];
            //memset(m_buffer, 32, m_bufferSize);
            ReadProcessMemory(m_hProc, (LPCVOID)from, m_buffer, sizeof(LPVOID), 0); //work
      }
      float toFloat()
      {
        float v = 0;
        memcpy(&v, m_buffer, sizeof(float));
        return v;
      }
}
#pragma package(smart_init)
