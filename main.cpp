#include <windows.h>
#include "stdio.h"
#include <vector>
#include <string>
#include "class.com.h"
#include <TLHelp32.h>
#include <cstdlib>

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
char szClassName[ ] = "WindowsApp";

HWND textBox,textBox2,textBox3,textBox4,textBox5,scrr1,currentComPortLabel;
HWND currentSpeedLabel, currentSpeedGameLabel, currentAddress;

int min = 85;
int max = 213;
int minSpeedPribor = 20;
int maxSpeedPribor = 160;

char* intToChar(int number) {
        char* ch = new char;
        sprintf ( ch, "%d", number ) ;
        return ch;
}
class ClassCom com;

char * currentComPort;
void findComPort() {
     for(int i=1;i<10;i++) {
        char* ch = new char;
        sprintf ( ch, "COM%d", i ) ;
        com.Connect(ch, 9600);
        
        std::vector<unsigned char> devector;
        devector.push_back(49);
        com.Write(devector);
        
        std::vector<unsigned char> the_vectsor;
        the_vectsor.push_back(5);
        com.Read(the_vectsor);
        if((int)the_vectsor[0] == 49) {
            currentComPort = ch;
            
            char buff[100];

            sprintf( buff, "������������: COM%d", i);
            SetWindowText(currentComPortLabel, buff);
            break;
        }
        
     }
     
}

int WINAPI WinMain (HINSTANCE hThisInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nFunsterStil)

{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default color as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           "������� ����� - ���������",       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           390,                 /* The programs width */
           350,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nFunsterStil);
    
    HWND currentSpeedLabel2 = CreateWindow("static", "min/volt",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              10, 40, 60, 20,
                              hwnd, (HMENU)(503),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
    HWND currentSpeedLabel3 = CreateWindow("static", "max/volt",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              80, 40, 60, 20,
                              hwnd, (HMENU)(504),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
    HWND currentSpeedLabel4 = CreateWindow("static", "min/pribor",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              150, 40, 100, 20,
                              hwnd, (HMENU)(505),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
    HWND currentSpeedLabel5 = CreateWindow("static", "max/pribor",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              260, 40, 100, 20,
                              hwnd, (HMENU)(506),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
   currentComPortLabel = CreateWindow("static", "������������: COM?",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              10, 130, 160, 20,
                              hwnd, (HMENU)(507),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
   currentSpeedGameLabel = CreateWindow("static", "������� ��������: 0 km/h",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              190, 160, 260, 20,
                              hwnd, (HMENU)(508),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
        
    currentAddress = CreateWindow(("EDIT"),"0x19D2BC88",WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        10,160,120,20,hwnd,(HMENU)700,NULL,NULL);
        
    textBox = CreateWindow(("EDIT"),intToChar(20),WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        10,10,160,20,hwnd,(HMENU)5001,NULL,NULL);
        
    textBox2 = CreateWindow(("EDIT"),intToChar(min),WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        10,60,60,20,hwnd,(HMENU)5002,NULL,NULL);
    
    textBox3 = CreateWindow(("EDIT"),intToChar(max),WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        80,60,60,20,hwnd,(HMENU)5003,NULL,NULL);
        
    textBox4 = CreateWindow(("EDIT"),intToChar(minSpeedPribor),WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        150,60,100,20,hwnd,(HMENU)5004,NULL,NULL);
        
    textBox5 = CreateWindow(("EDIT"),intToChar(maxSpeedPribor),WS_BORDER|WS_VISIBLE|WS_CHILD|ES_LEFT|ES_MULTILINE,
        260,60,100,20,hwnd,(HMENU)5005,NULL,NULL);
        
        //SetWindowText(textBox,intToChar(0));
        
    HWND hButton = CreateWindow("BUTTON", "����������", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
        180, 10, 80, 20, hwnd, (HMENU)500, NULL, NULL);
        
    HWND hButton2 = CreateWindow("BUTTON", "Go", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
        140, 160, 40, 20, hwnd, (HMENU)502, NULL, NULL);
        
    currentSpeedLabel = CreateWindow("static", "Speed: 0 km/h",
                              WS_CHILD | WS_VISIBLE | WS_TABSTOP,
                              270, 12, 200, 20,
                              hwnd, (HMENU)(502),
                              (HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), NULL);
                              
    scrr1= CreateWindow(("scrollbar"), NULL, WS_CHILD| WS_VISIBLE, 
                10, 100, 350, 20, hwnd,  (HMENU)600, NULL, NULL);
                
    int s1Pos=20;int s1Min=1;int s1Max=255-22;
            SetScrollRange(scrr1,SB_CTL,s1Min,s1Max,TRUE);
            SetScrollPos(scrr1,SB_CTL,s1Pos,TRUE);
            
    findComPort();

        
    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}

void blinkLed(int count, int time) {
     for(int i=0;i<count;i++) {
         std::vector<unsigned char> devector;
         devector.push_back(49);
         com.Write(devector);
         Sleep(time);
    
         devector.push_back(50);
         com.Write(devector);
         Sleep(time);
     }
}

char* currentSpeed = new TCHAR[150];

void sendCommand(int command) {
     std::vector<unsigned char> devector;
     devector.push_back(command);
     com.Write(devector);
}

void buttonAction() {
     
     GetWindowText(textBox,currentSpeed, 150);
     
     int i = atoi (currentSpeed);
     int startSpeed = minSpeedPribor;
     if(i >= (255 - startSpeed)) {
         i = (255 - startSpeed - 2);
     }
     
     int labelSpeed = i;
     int diff = max - min;
     
 
     if(i >= startSpeed) {
           i=min;
           
           float rating = ((float)diff/maxSpeedPribor) * (labelSpeed - minSpeedPribor);
           i+=(int)rating; 
     } else {
           i=0;
           
     }
     sendCommand(i);
    
     char buff[100];

     sprintf( buff, "Speed: %d km/h", labelSpeed);
     SetWindowText(currentSpeedLabel, buff);
     //MessageBox( NULL, buff, "", MB_OK ); 
}


char* m_pName = "eurotrucks2.exe";
int m_pID = 0;
HANDLE m_hProc;
byte*  m_buffer;
size_t m_bufferSize;

float toFloat()
{
        float v = 0;
        memcpy(&v, m_buffer, sizeof(float));
        return v;
}

char* toStringA()
{
        return (char*)m_buffer;
}

void initMem() {
     HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        PROCESSENTRY32 pInfo;
        pInfo.dwSize = sizeof(PROCESSENTRY32);
 
        if (Process32First(snapshot, &pInfo))
        {
                while (Process32Next(snapshot, &pInfo))
                {
                        if (_stricmp(m_pName, pInfo.szExeFile) == 0)
                        {
                                m_pID = pInfo.th32ProcessID;
                                CloseHandle(snapshot);
                                return;
                        }
                }
        }
        CloseHandle(snapshot);
        m_pID = 0;

        
}
void Open(DWORD accessRights = PROCESS_VM_READ)
{       accessRights = PROCESS_ALL_ACCESS;
        m_hProc = OpenProcess(accessRights, false, m_pID);
}
void Read(DWORD from, size_t size)
{
        m_buffer = new byte[size];
        //memset(m_buffer, 32, m_bufferSize);
        ReadProcessMemory(m_hProc, (LPCVOID)from, m_buffer, sizeof(LPVOID), 0); //work
}

#define THREADS_NUMBER 10
DWORD dwCounter = 0;
DWORD WINAPI ThreadProc(CONST LPVOID lpParam) {
  CONST HANDLE hMutex = (CONST HANDLE)lpParam;
  DWORD i;
  
     char* curAddr = new TCHAR[150];
  
     GetWindowText(currentAddress,curAddr, 150);
     DWORD from = std::strtoul(curAddr, NULL, 16);
  
  while(1) {
     Read(from, 32);
     int speed = (int)toFloat();
     speed = speed * 365 / 100;
     
     char buff[100];
     sprintf( buff, "������� ��������: %d km/h", speed);
     SetWindowText(currentSpeedGameLabel, buff);
     SetWindowText(textBox, intToChar(speed));
     buttonAction();
    Sleep(30);
  }
  ExitThread(0);
}

void changeSpeed() {
     
     if(m_pID == 0 ) {
              initMem();
              Open();
     }
     
     
     HANDLE hThreads[THREADS_NUMBER];
     CONST HANDLE hMutex = CreateMutex(NULL, FALSE, NULL);
     for(int i = 0; i < THREADS_NUMBER; i++) {
             hThreads[i] = CreateThread(NULL, 0, &ThreadProc, hMutex, 0, NULL);
     }
}

void updateParams(WPARAM wParam) {
     char* buff = new TCHAR[150];
     int i;
     if(wParam == 5002) {
         GetWindowText(textBox2,buff, 150);
         i = atoi (buff);
         min = i;
     }
     if(wParam == 5003) {
         GetWindowText(textBox3,buff, 150);
         i = atoi (buff);
         max = i;
     }
     if(wParam == 5004) {
         GetWindowText(textBox4,buff, 150);
         i = atoi (buff);
         minSpeedPribor = i;
     }
     if(wParam == 5005) {
         GetWindowText(textBox5,buff, 150);
         i = atoi (buff);
         maxSpeedPribor = i;
     }
     SetWindowText(currentSpeedLabel, intToChar(i));
     
     buttonAction();
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_COMMAND:
             if(LOWORD(wParam)==500) {
                 buttonAction();
             }
             if(LOWORD(wParam) >= 5002 && LOWORD(wParam) <= 5005) {
                 updateParams(LOWORD(wParam));
             }
             if(LOWORD(wParam) == 502) {
                 changeSpeed();      
             }
             break;
        case WM_HSCROLL:
             if((HWND)lParam==scrr1)
             {
                                    static int s1Pos,s1Min,s1Max,s2Pos,s2Min,s2Max;
                  switch(LOWORD(wParam)) // ���-�� � ��� ���� ������ �� ���� � ����
                  {
                    case SB_PAGERIGHT:
                        {s1Pos+=10;break;}
                    case SB_LINERIGHT:
                        {s1Pos+=1;break;}
                    case SB_PAGELEFT:
                        {s1Pos-=10;break;}
                    case SB_LINELEFT:
                        {s1Pos-=1;break;}
                    case SB_TOP:
                        {s1Pos=s1Min;break;}
                    case SB_BOTTOM:
                        {s1Pos=s1Max;break;}
                    case SB_THUMBPOSITION:
                    case SB_THUMBTRACK:
                        {s1Pos=HIWORD(wParam);break;}
                  }
                  SetScrollPos(scrr1,SB_CTL,s1Pos,TRUE);
                  SetWindowText(textBox, intToChar(s1Pos));
                  buttonAction();
             }      
             
             break;
        case WM_DESTROY:
            SetWindowText(textBox, intToChar(0));
            buttonAction();
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}
