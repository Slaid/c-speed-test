/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef PEED_PRIVATE_H
#define PEED_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.1.1.252"
#define VER_MAJOR	0
#define VER_MINOR	1
#define VER_RELEASE	1
#define VER_BUILD	252
#define COMPANY_NAME	"Aferon.com"
#define FILE_VERSION	"v228"
#define FILE_DESCRIPTION	"Developed using the Dev-C++ IDE"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	"copy aferon.com"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	""
#define PRODUCT_NAME	"Garaj production"
#define PRODUCT_VERSION	"v228"

#endif /*PEED_PRIVATE_H*/
